import React from 'react';
import styled from 'styled-components';
import SvgSearch from "../../media/js/Search";
import SvgGoogle from "../../media/js/Google";
import SvgBing from "../../media/js/Bing";

const barWidth = "80vw";
const barHeight = "6vw";

const Container = styled.div `
  width: ${barWidth};
  height: ${barHeight};
`
const Layout = styled.div `
  width: calc(${barWidth} - 4vw);
  padding: 0 2vw;
  height: ${barHeight};
  -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.55);
  -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.55);
  box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.55);
  background-color: white;
  border-radius: 12px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`
const SelectorButton = styled.div `
  background-color: #dfdfdf;
  border-radius: 30px;
  display: flex;
  width: calc(${barWidth} * 0.070);
  padding: 0 calc(${barWidth} * 0.015);
  height: calc(${barHeight} * 0.75);
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  opacity: 1;
  &:hover {
    opacity: 0.6;
  }
`
const SearchIconContainer = styled.div `
  margin-right: 2vw;
  cursor: pointer;
  opacity: 1;
  &:hover {
    opacity: 0.6;
  }
`
const Input = styled.input `
  width: calc(${barWidth} * 0.78);;
  height: calc(${barHeight} * 0.4 );
  padding: calc(${barHeight} * 0.2 ) 0;
  color: #333348;
  font-size: 2vw;
  border: none;
  outline: none;
`
const RightCol = styled.div `
  display: flex;
  align-items: center;
`
const SelectedNavIcon = styled.div `
  background-color: ${({google, activated}) => activated ? (google ? `#333348`: `#e13214`) : ''};
  padding: 0.3vw;
  border-radius: 2vw;
  display: flex;
  justify-content: center;
  align-items: center;
`
const SearchBar = (props) => {
  const handleKeyUp = (event) => {
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      props.handleSearch();
    }
  }
  return (
    <Container>
      <Layout >
        <Input
          onFocus={props.onFocus}
          onBlur={props.onBlur}
          onChange={props.onChange}
          placeholder={"Avantica Search"}
          value={props.value}
          onKeyUp={handleKeyUp}
        />
        <RightCol>
          <SearchIconContainer onClick={props.handleSearch}>
            <SvgSearch
              fill={"#b2b2b2"}
              width={"2.7vw"}
              height={"2.7vw"}
            />
          </SearchIconContainer>
          <SelectorButton onClick={props.browserOnClick} >
            <SelectedNavIcon activated={props.browser === 0 || props.browser === 2} google>
              <SvgGoogle
                width={"2vw"}
                height={"2vw"}
              />
            </SelectedNavIcon>
            <SelectedNavIcon activated={props.browser === 1 || props.browser === 2}>
              <SvgBing
                width={"2vw"}
                height={"2vw"}
              />
            </SelectedNavIcon>
          </SelectorButton>
        </RightCol>
      </Layout>
    </Container>
  )
}
SearchBar.defaultProps = {
  value: "",
  onChange: () => {},
  browser: 0,
  browserOnClick: () => {},
  handleSearch: () => {}
}
export default SearchBar;
