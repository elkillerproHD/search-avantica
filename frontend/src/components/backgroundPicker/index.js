import React from 'react';
import styled from 'styled-components';
import SvgBack from "../../media/js/Back";
import SvgNext from "../../media/js/Next";
import SvgImage from "../../media/js/Image";

const Container = styled.div `
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  position: relative;
`
const Text = styled.p `
  font-family: Montserrat;
  font-size: 20px;
  color: #ffffff;
  margin-left: 1.5vw;
  margin-right: 1.5vw;
`
const ImagePickerLayout = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: -2vw;
  right: 1vw;
`
const ImagePickerText = styled.p `
  font-family: Montserrat;
  font-size: 12px;
  color: #ffffff;
`
const BackgroundPicker = (props) => {
  return(
    <Container>
      <SvgBack onClick={props.PrevBackground} style={{cursor: "pointer"}} fill={"#fff"} width={"1.5vw"} height={"1.5vw"} />
      <Text>Background { props.backgroundImage }</Text>
      <SvgNext onClick={props.NextBackground} style={{cursor: "pointer"}} fill={"#fff"} width={"1.5vw"} height={"1.5vw"} />
      <ImagePickerLayout>
        <ImagePickerText>Images</ImagePickerText>
        <SvgImage style={{paddingTop: "0.3vw", marginLeft: "0.4vw"}} fill={"#fff"} width={"1vw"} height={"1vw"} />
      </ImagePickerLayout>
    </Container>
  )
}
export default BackgroundPicker
