import React from 'react';
import styled from 'styled-components';

const Container = styled.div `
  width: 26vw;
  height: 15vw;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-left: 2vw;
  margin-right: 2vw;
`
const SquareContainer = styled.div `
  width: 21vw;
  height: 10vw;
  background-color: rgba(34,34,34,.9);
  position: relative;
  cursor: pointer;
  padding: 2vw;
  border-radius: 20px;
  display: flex;
`
const ImgContainer = styled.div `
  width: 10vw;
  height: 10vw;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Square = styled.img `
  width: 20vw;
  height: 10vw;
`
const Text = styled.p `
  width: 50%;
  font-family: Montserrat;
  font-size: 12px;
  text-align: left;
  color: #fff;
  margin-left: 1vw;
  cursor: pointer;
  &:hover {
    color: #8b8dff;
  }
`


const NewsSingle = (props) => {
  return(
    <Container>
      <SquareContainer onClick={()=> window.open(props.url, "_blank")}>
        <ImgContainer>
          <Square src={props.urlToImage} />
        </ImgContainer>
        <Text onClick={()=> window.open(props.url, "_blank")} >{props.title}</Text>
      </SquareContainer>
    </Container>
  )
}
export default NewsSingle;
