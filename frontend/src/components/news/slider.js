import React from 'react';
import styled from 'styled-components';
import NewsSingle from "./single";
import Data from '../../news/news.json';

const Container = styled.div `
  display: flex;
  flex-direction: column;
  margin-bottom: 2vw;
`

const SubContainer = styled.div `
  width: 100vw;
  height: 15vw;
  display: flex;
  overflow: auto;
  overflow-y: hidden;
  padding-bottom: 2vw;
  ::-webkit-scrollbar {
    height: 8px;
  }
  
  /* Track */
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }
   
  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #dfdfdf; 
    border-radius: 10px;
  }
  
  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #d2d2d2; 
  }
`
const EmptySpace = styled.div `
  height: 18vw;
  width: ${window.innerWidth * 0.04}px;
  min-width: ${window.innerWidth * 0.04}px;
`
const Title = styled.p `
  font-family: Montserrat;
  font-size: 20px;
  text-align: left;
  color: #fff;
  margin-left: 4vw;
  margin-bottom: 2vw;
`
const NewsSlider = (props) => {
  return (
    <Container>
      <Title>News</Title>
      <SubContainer>
        {
          Data.articles.map((article, index) => {
            return (
              <NewsSingle {...article} key={`DataArticle${index}`} />
            )
          })
        }
        <EmptySpace />
      </SubContainer>
    </Container>
  )
}
export default NewsSlider;
