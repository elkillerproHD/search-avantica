import React from 'react';
import styled from 'styled-components';
import SpectrumAnalyzer from "./spectrumAnalyzer";
import AudioMotionAnalyzer from "audiomotion-analyzer";
import SvgBackSound from "../../media/js/BackSound";
import SvgNextSound from "../../media/js/NextSound";
import SvgPause from "../../media/js/Pause";
import SvgPlay from "../../media/js/Play";
import SvgVolume from "../../media/js/Volume";
import SvgNoSound from "../../media/js/NoSound";
import Music1 from "../../media/music/2 Souls - Lonely (ft. Nara) [NCS Release].mp3";
import Music2 from "../../media/music/Lioness (Instrumental) - DayFox [Free Copyright-safe Music].mp3";
import Music3 from "../../media/music/MGJ - Dynamite (Copyright Free Music Release) Royalty Free.mp3";
import Music4 from "../../media/music/Jim Yosef x ROY KNOX - Sun Goes Down [NCS Release].mp3";
import Music5 from "../../media/music/Leonell Cassio - Sittin' Throwin' Rocks (ft. Lily Hain) [Copyright FreeFree To Use].mp3";
import Music6 from "../../media/music/Leonell Cassio - Night Sky (Lyrics) ft. Julia Mihevc.mp3";
import Music7 from "../../media/music/Diamond Eyes - Gravity [NCS Release].mp3";
import Music8 from "../../media/music/musicbyLUKAS x Mastrovita - Hold On (Copyright Free Music).mp3";
import Music9 from "../../media/music/EMDI - Hurts Like This (feat. Veronica Bravo) [NCS Release].mp3";
import Music10 from "../../media/music/Cartoon - Howling (Ft. Asena) [NCS Release].mp3";
import Music11 from "../../media/music/Tobu - Turn It Up [NCS Release].mp3";
import Music12 from "../../media/music/Leonell Cassio - Help You Out (Lyrics) ft. Jonathon Robbins.mp3";
import Music13 from "../../media/music/Ellis - Clear My Head [NCS Release].mp3";
import Music14 from "../../media/music/Syn Cole - Gizmo [NCS Release].mp3";
import Music15 from "../../media/music/Diamond Eyes - Everything [NCS Release].mp3";
import Music16 from "../../media/music/Leonell Cassio - Why'd You Wanna End (ft. Alex Lippett) [LYRIC VIDEO] Free To UseCopyright Free.mp3";
import Music17 from "../../media/music/Rival x Egzod - Live A Lie (ft. Andreas Stone) [NCS Release].mp3";
import Music18 from "../../media/music/Lost Sky - Where We Started (feat. Jex) [NCS Release].mp3";
import Music19 from "../../media/music/Audioscribe - Shimmer [NCS Release].mp3";

const Container = styled.div `
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 110px;
  background-color: rgba(0,0,0,.95);
`
const SubContainer = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0,0,0,.98);
  border-radius: 50px;
`
const Text = styled.p `
  font-family: Montserrat;
  font-size: 25px;
  text-align: left;
  color: #ffffff;
`
const Audio = styled.audio `
  display: none;
`
const Col = styled.div `
  margin-left: 1vw;
  margin-right: 1vw;
  display: flex;
  justify-content: center;
  align-items: center;
`
const idAud = "audio";
const idSpec = "SpectrumAnalizer-Container";
const ControlIcon = styled.div `
  cursor:pointer;
  margin: 0 5px 0 5px;
`

const Playlist = [
  {
    name: "2 Souls - Lonely (ft. Nara)",
    file: Music1
  },
  {
    name: "Lioness (Instrumental) — DayFox",
    file: Music2
  },
  {
    name: "MGJ - Dynamite",
    file: Music3
  },
  {
    name: "Jim Yosef x ROY KNOX - Sun Goes Down",
    file: Music4
  },
  {
    name: "Leonell Cassio - Sittin' Throwin' Rocks (ft. Lily Hain)",
    file: Music5
  },
  {
    name: "Leonell Cassio - Night Sky (ft. Julia Mihevc)",
    file: Music6
  },
  {
    name: "Diamond Eyes - Gravity",
    file: Music7
  },
  {
    name: "musicbyLUKAS X Mastrovita - Hold On",
    file: Music8
  },
  {
    name: "EMDI - Hurts Like This (feat. Veronica Bravo)",
    file: Music9
  },
  {
    name: "Cartoon - Howling (Ft. Asena)",
    file: Music10
  },
  {
    name: "Tobu - Turn It Up [NCS Release]",
    file: Music11
  },
  {
    name: "Leonell Cassio - Help You Out (ft. Jonathon Robbins)",
    file: Music12
  },
  {
    name: "Ellis - Clear My Head",
    file: Music13
  },
  {
    name: "Syn Cole - Gizmo",
    file: Music14
  },
  {
    name: "Diamond Eyes - Everything",
    file: Music15
  },
  {
    name: "Leonell Cassio - Why'd You Wanna End (ft. Alex Lippett)",
    file: Music16
  },
  {
    name: "Rival x Egzod - Live A Lie (ft. Andreas Stone)",
    file: Music17
  },
  {
    name: "Lost Sky - Where We Started (feat. Jex)",
    file: Music18
  },
  {
    name: "Audioscribe - Shimmer",
    file: Music19
  },
]

class AudioPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      play: true,
      sound: true,
      playListNumber: 0,
    }
  }
  componentDidMount() {
    this.DeclareAudio();
    const {state} = this;
    if(state.sound) {
      setTimeout(() => {
        this.setState({
          play: true
        })
        const compAud = document.getElementById(idAud);
        compAud.play();
      }, 500)
    } else {
      const compAud = document.getElementById(idAud);
      compAud.muted = true;
    }
  }

  DeclareAudio = () => {
    const compAud = document.getElementById(idAud);
    const audioMotion = new AudioMotionAnalyzer(
      document.getElementById(idSpec),
      {
        source: compAud,
        radial: true,
        mode: 7,
        minDecibels: -85,
        smoothing: 0.1,
        maxFreq: 20000,
        minFreq: 10,
        spinSpeed: 5,
        showPeaks: false,
        showScale: false,
        width: 150,
        height: 150
      }
    );
  }
  handlePlayPause = () => {
    const { play } = this.state;

    if(play === false){
      console.log("Play")
      this.setState({
        play: true
      })
      setTimeout(() => {
        const compAud = document.getElementById(idAud);
        compAud.play();
      }, 200)
    } else {
      console.log("pause")
      this.setState({
        play: false
      })
      setTimeout(() => {
        const compAud = document.getElementById(idAud);
        compAud.pause();
      }, 200)
    }
  }
  handleToggleSound = () => {
    const { sound } = this.state;
    this.setState({
      sound: !sound
    })
    const compAud = document.getElementById(idAud);
    if(sound){
      compAud.muted = true;
    } else {
      compAud.muted = false;
    }
  }
  nextPress = () => {
    const { playListNumber } = this.state;
    if(playListNumber === Playlist.length - 1){
      this.setState({
        playListNumber: 0
      })
    } else {
      this.setState({
        playListNumber: playListNumber + 1
      })
    }
    setTimeout(() => {
      const compAud = document.getElementById(idAud);
      compAud.play();
    }, 200)
  }
  backPress = () => {
    const { playListNumber } = this.state;
    if(playListNumber === 0){
      this.setState({
        playListNumber: Playlist.length - 1
      })
    } else {
      this.setState({
        playListNumber: playListNumber - 1
      })
    }
    setTimeout(() => {
      const compAud = document.getElementById(idAud);
      compAud.play();
    }, 200)
  }
  render(){
    const { play, sound, playListNumber } = this.state;
    return (
      <Container>
        <SubContainer>
          <Col>
            <Text>Music</Text>
            <Audio
              id={idAud}
              src={Playlist[playListNumber].file}
              onEnded={this.nextPress}
              controls
              autoplay
            />
            <SpectrumAnalyzer />
          </Col>
          <Col>
            <ControlIcon onClick={this.backPress}>
              <SvgBackSound width={"20px"} height={"20px"} fill={"white"} />
            </ControlIcon>
            <ControlIcon onClick={this.handlePlayPause}>
              {
                play
                  ? (
                    <SvgPause style={{marginLeft: 20, marginRight: 20}} width={"20px"} height={"20px"} fill={"white"} />
                  )
                  : (
                    <SvgPlay style={{marginLeft: 20, marginRight: 20}} width={"20px"} height={"20px"} fill={"white"} />
                  )
              }
            </ControlIcon>
            <ControlIcon onClick={this.nextPress}>
              <SvgNextSound width={"20px"} height={"20px"} fill={"white"} />
            </ControlIcon>
          </Col>
          <Col>
            <Text style={{marginRight: 20, paddingBottom: 5}}>{Playlist[playListNumber].name}</Text>
            <ControlIcon onClick={this.handleToggleSound} style={{paddingRight: 4}}>
              {
                sound
                  ? (
                    <SvgVolume style={{ marginRight: -4, paddingLeft: 4 }} width={"25px"} height={"25px"} fill={"white"}/>
                  )
                  : (
                    <SvgNoSound width={"25px"} height={"25px"} fill={"white"}/>
                  )
              }
            </ControlIcon>
          </Col>
        </SubContainer>
      </Container>
    )
  }
}
export default AudioPlayer;
