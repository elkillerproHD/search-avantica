import React from 'react';
import styled from 'styled-components';

const Container = styled.div `
  width: 100px;
  height: 100px;
`
const idSpec = "SpectrumAnalizer-Container";

class SpectrumAnalyzer extends React.Component {

  render() {
    return(
      <Container id={idSpec}>
      </Container>
    )
  }
}
export default SpectrumAnalyzer;
