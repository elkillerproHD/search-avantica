const spectrumColorAnalyzer = {
  bgColor: 'black',
  colorStops: [
    'hsl( 360, 100%, 100% )',
    'hsl(7,85%,58%)',
    'hsl(240,100%,60%)'
  ]
}
