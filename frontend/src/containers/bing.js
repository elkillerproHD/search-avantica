import React from 'react';
import styled from 'styled-components';
import { connect } from "react-redux";
import {GeneralReducerName} from "../redux/general";
import SvgBing from "../media/js/Bing";
import SvgBack from "../media/js/Back";
import {BackSearch} from "../redux/general/action";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`
const BrowserCol = styled.div `
  width: 85vw;
  height: 75vh;
  padding: 10vh 5vw;
`
const TitleResults = styled.div `
  display: flex;
  width: 100%;
  justify-content: flex-start;
  align-items: center;
`
const Text = styled.p `
  font-family: Montserrat;
  font-size: 25px;
  text-align: left;
  color: #000;
  margin-left: 2vw;
`
const TitleWebpage = styled.div `
  color: #1a73e8;
  font-size: 20px;
  line-height: 24px;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`
const ContainerWebPage = styled.div `
  margin-bottom: 6vh;
  margin-top: 2vh;
`
const LinkWebPage = styled.div `
  color: #006621;
  font-size: 16px;
  line-height: 24px;
`
const Description = styled.div `
  color: #666;
  line-height: 20px;
  font-size: 13px;
`
class BingResultsPage extends React.Component {
  render() {
    const { props } = this;
    return(
      <Container>
        <SvgBack
          style={{
            position: "absolute",
            top: "7vw",
            left: "1vw",
            cursor: "pointer",
          }}
          onClick={() => props.dispatch(BackSearch())}
          width={"4vw"}
          height={"4vw"}
          fill={"#000"}
        />
        <BrowserCol >
          <TitleResults>
            <SvgBing
              width={"5vw"}
              height={"5vw"}
              style={{marginBottom: "3vh"}}
            />
            <Text>
              Bing Results
            </Text>
          </TitleResults>
          {
            props.bingResults.map((result) => {
              return (
                <ContainerWebPage>
                  <TitleWebpage>{result.name}</TitleWebpage>
                  <LinkWebPage>{result.displayUrl}</LinkWebPage>
                  <Description>{result.snippet}</Description>
                </ContainerWebPage>
              )
            })
          }
        </BrowserCol>
      </Container>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    ...{
      bingResults: state[GeneralReducerName].bingResults,
    },
  };
};
export default connect(mapStateToProps)(BingResultsPage);
