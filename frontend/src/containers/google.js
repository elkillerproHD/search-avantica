import React from 'react';
import styled from 'styled-components';
import { connect } from "react-redux";
import {GeneralReducerName} from "../redux/general";
import SvgGoogle from "../media/js/Google";
import SvgBack from "../media/js/Back";
import {BackSearch} from "../redux/general/action";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`
const BrowserCol = styled.div `
  width: 85vw;
  height: 75vh;
  padding: 10vh 5vw;
`
const TitleResults = styled.div `
  display: flex;
  width: 100%;
  justify-content: flex-start;
  align-items: center;
`
const Text = styled.p `
  font-family: Montserrat;
  font-size: 25px;
  text-align: left;
  color: #000;
  margin-left: 2vw;
`
class GoogleResultsPage extends React.Component {
  render() {
    const { props } = this;
    return(
      <Container>
        <SvgBack
          style={{
            position: "absolute",
            top: "7vw",
            left: "1vw",
            cursor: "pointer",
          }}
          onClick={() => props.dispatch(BackSearch())}
          width={"4vw"}
          height={"4vw"}
          fill={"#000"}
        />
        <BrowserCol >
          <TitleResults>
            <SvgGoogle
              width={"5vw"}
              height={"5vw"}
              style={{marginBottom: "3vh"}}
            />
            <Text>
              Google Results
            </Text>
          </TitleResults>
          <div id={"main"}>
            {
              props.googleResults.map((result) => {
                return(
                  <div dangerouslySetInnerHTML={{
                    __html: result.innerHTML
                  }} />
                )
              })
            }
          </div>
        </BrowserCol>
      </Container>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    ...{
      googleResults: state[GeneralReducerName].googleResults,
    },
  };
};
export default connect(mapStateToProps)(GoogleResultsPage);
