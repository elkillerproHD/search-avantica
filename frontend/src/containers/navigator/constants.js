export const VIEWS_CONSTANTS = {
  searchPage: "SEARCH_PAGE",
  resultPage: "RESULT_PAGE",
  bingResultPage: "BING_RESULT_PAGE",
  googleResultPage: "GOOGLE_RESULT_PAGE"
}
