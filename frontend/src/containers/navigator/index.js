import React from 'react';
import { connect } from 'react-redux';
import {VIEWS_PAGES} from "./views";
import {VIEWS_CONSTANTS} from "./constants";
import {GeneralReducerName} from "../../redux/general";

class Navigator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      view: VIEWS_CONSTANTS.searchPage
    }
  }
  render () {
    const {view} = this.props;
    return VIEWS_PAGES[view];
  }
}
const mapStateToProps = (state) => {
  return {
    ...{
      view: state[GeneralReducerName].view,
    },
  };
};
export default connect(mapStateToProps)(Navigator);
