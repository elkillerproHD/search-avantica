import React from "react";
import { VIEWS_CONSTANTS } from './constants'
import SearchPage from "../search";
import ResultsPage from "../results";
import BingResultsPage from "../bing";
import GoogleResultsPage from "../google";
export const VIEWS_PAGES = {
  [VIEWS_CONSTANTS.searchPage]: <SearchPage />,
  [VIEWS_CONSTANTS.resultPage]: <ResultsPage />,
  [VIEWS_CONSTANTS.bingResultPage]: <BingResultsPage />,
  [VIEWS_CONSTANTS.googleResultPage]: <GoogleResultsPage />,
}
