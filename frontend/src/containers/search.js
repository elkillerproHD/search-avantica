import React from 'react';
import styled, {css, keyframes} from 'styled-components';
import AudioPlayer from "../components/audioPlayer";
import NewsSlider from "../components/news/slider";
import BackgroundPicker from "../components/backgroundPicker";
import SvgAvantSearch from "../media/js/AvantSearch";
import {GeneralReducerName} from "../redux/general";
import {connect} from "react-redux";

import Background1 from '../media/images/background1.jpg'
import Background2 from '../media/images/background2.jpg'
import Background3 from '../media/images/background3.jpg'
import Background4 from '../media/images/background4.jpg'
import Background5 from '../media/images/background5.jpg'
import Background6 from '../media/images/background6.jpg'
import Background7 from '../media/images/background7.jpg'
import {
  NextBackgroundImage,
  PrevBackgroundImage,
  SetBingResults,
  SetGoogleResults, SetSearchWord,
  SetView
} from "../redux/general/action";
import SearchBar from "../components/searchBar";
import {VIEWS_CONSTANTS} from "./navigator/constants";
import BingTest from '../news/bingTest.json';

const BackgroundImages = [
  Background1,
  Background2,
  Background3,
  Background4,
  Background5,
  Background6,
  Background7,
]

const EmptyGoogleResults = styled.div `
  display: none;
`

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  background-image: url("${({backgroundImage}) => BackgroundImages[backgroundImage]}");
  background-size: 100vw 100vh;
`
const Header = styled.div `
  width: 98vw;
  position: absolute;
  left: 0;
  top: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 2vw;
  padding-left: 2vw;
  z-index: 1;
`
const EmptyHeaderPart = styled.div `
  width: 28%;
  height: 100%;
`
const SearchBarContainer = styled.div `
  margin-bottom: 2vw;
  z-index: 3;
`
const FadeIn = keyframes`
  from {
  opacity: 0;
  }

  to {
    opacity: 1;
  }
`;
const Shadow = styled.div `
  width: 100vw;
  height: 100vh;
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: rgba(0,0,0,0.6);
  ${({modal}) => !modal && (`
  opacity: 0;
  z-index: 0;
  `)}
  transition: opacity 1s ease;
  animation: ${({modal}) => modal ? (css` ${FadeIn} 1s ease;`) : ''}}
`
const SliderAndAudioPlayLayout = styled.div `
  z-index: 1;
`
class SearchPage extends React.Component{
  state = {
    modal: false,
    browser: 0,
    searchWord: ''
  }
  OnChangeSearchWord = (e) => {
    this.setState({
      searchWord: e.target.value
    })
  }
  AppendGoogleStylesToHead = (stylesTag) => {
    const headTag = document.getElementsByTagName("head")
    console.log(headTag);
    headTag[0].appendChild(stylesTag);
  }
  GetUsableValuesOfGoogleResponse = (data) => {
    const { props } = this;
    const toAppend = document.getElementById("EmptyGoogleResults")
    toAppend.innerHTML = data;
    const children = toAppend.children[0].children
    this.AppendGoogleStylesToHead(children[1]);
    const usableValues = [];
    for (let i = 3; i < children.length; i++) {
      if(children[i].classList.length === 0){
        usableValues.push(children[i])
      }
    }
    props.dispatch(SetGoogleResults(usableValues));
    console.log(usableValues);
  }
  GetUsableValuesOfBingResponse = (data) => {
    const { props } = this;
    const toAppend = document.getElementById("EmptyBingResults")
    console.log(data.webPages.value)
    props.dispatch(SetBingResults(data.webPages.value))
    // const children = toAppend.children[0].children;
    // console.log(data)
  }
  onHandleSearch = () => {
    const { state, props } = this;
    console.log("onHandleSearch")
    if(state.searchWord){
      if(state.searchWord.length > 0){
        props.dispatch(SetSearchWord(state.searchWord))
        if(state.browser === 0){
          this.SearchInGoogle();
        } else if(state.browser === 1) {
          this.SearchInBing();
        } else if(state.browser === 2) {
          this.SearchInBingAndGoogle();
        }
      }
    }
  }
  SearchInGoogle = () => {
    const { state, props } = this;
    fetch("http://localhost:3001/google?q=" + state.searchWord, )
      .then( (response) => {
      // The API call was successful!
      return response.json();
    }).then( (json) => {
      this.GetUsableValuesOfGoogleResponse(json.content)
      props.dispatch(SetView(VIEWS_CONSTANTS.googleResultPage));
    }).catch((err) => {
      // There was an error
      console.warn('Something went wrong.', err);
    });
  }
  SearchInBing = () => {
    const { state, props } = this;
    fetch("http://localhost:3001/bing?q=" + state.searchWord, )
      .then( (response) => {
      // The API call was successful!
      return response.json();
    }).then( (json) => {
      this.GetUsableValuesOfBingResponse(json.content)
      props.dispatch(SetView(VIEWS_CONSTANTS.bingResultPage));
    }).catch((err) => {
      // There was an error
      console.warn('Something went wrong.', err);
    });
  }
  SearchInBingAndGoogle = () => {
    const { state, props } = this;
    fetch("http://localhost:3001/bing_google?q=" + state.searchWord, )
      .then( (response) => {
      // The API call was successful!
      return response.json();
    }).then( (json) => {
      this.GetUsableValuesOfGoogleResponse(json.google)
      this.GetUsableValuesOfBingResponse(json.bing)
      props.dispatch(SetView(VIEWS_CONSTANTS.resultPage));
    }).catch((err) => {
      // There was an error
      console.warn('Something went wrong.', err);
    });
  }
  componentDidMount() {

  }

  NextBackground = () => {
    const { dispatch } = this.props;
    dispatch(NextBackgroundImage());
  }
  PrevBackground = () => {
    const { dispatch } = this.props;
    dispatch(PrevBackgroundImage());
  }
  handleBrowserChange = () => {
    const { state } = this;
    switch (state.browser){
      case 0: this.setState({browser: 1}); break;
      case 1: this.setState({browser: 2}); break;
      case 2: this.setState({browser: 0}); break;
      default: break;
    }
  }
  render() {
    const { props, state } = this;
    return (
      <Container backgroundImage={props.backgroundImage} >

        <Header>
          <BackgroundPicker
            NextBackground={this.NextBackground}
            PrevBackground={this.PrevBackground}
            backgroundImage={props.backgroundImage}
          />
          <SvgAvantSearch />
          <EmptyHeaderPart />
        </Header>
        <EmptyGoogleResults id={"EmptyGoogleResults"} ></EmptyGoogleResults>
        <EmptyGoogleResults id={"EmptyBingResults"} ></EmptyGoogleResults>
        <SearchBarContainer>
          <SearchBar
            onFocus={() => {this.setState({modal: true})}}
            onBlur={() => {this.setState({modal: false})}}
            browser={state.browser}
            browserOnClick={this.handleBrowserChange}
            value={state.searchWord}
            onChange={this.OnChangeSearchWord}
            handleSearch={this.onHandleSearch}
          />
        </SearchBarContainer>
        <SliderAndAudioPlayLayout>
          <NewsSlider />
          <AudioPlayer />
        </SliderAndAudioPlayLayout>
        <Shadow browserOnClick={this.handleBrowserChange} modal={state.modal}/>
      </Container>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    ...{
      backgroundImage: state[GeneralReducerName].backgroundImage,
    },
  };
};
export default connect(mapStateToProps)(SearchPage);
