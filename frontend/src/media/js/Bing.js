import * as React from "react";

function SvgBing(props) {
  return (
    <svg width={64} height={64} viewBox="0 0 32 32" {...props}>
      <path
        d="M6.1 0l6.392 2.25v22.5l9.004-5.198-4.414-2.07-2.785-6.932 14.186 4.984v7.246L12.497 32 6.1 28.442z"
        fill="#008373"
      />
    </svg>
  );
}

export default SvgBing;
