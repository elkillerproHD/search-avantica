import * as React from "react";

function SvgBackSound(props) {
  return (
    <svg width={512} height={512} viewBox="0 0 416.004 416.004" {...props}>
      <g transform="matrix(-1 0 0 1 416.004 0)">
        <link
          type="text/css"
          rel="stylesheet"
          id="backSound_svg__dark-mode-custom-link"
        />
        <style type="text/css" id="dark-mode-custom-style" />
        <path
          d="M281.602 195.204l-256-192C20.802-.444 14.274-1.02 8.866 1.7A15.997 15.997 0 00.002 16.004v384a16.026 16.026 0 008.832 14.304 16.26 16.26 0 007.168 1.696c3.392 0 6.784-1.088 9.6-3.2l256-192c4.032-3.008 6.4-7.776 6.4-12.8s-2.368-9.792-6.4-12.8zM400.002.004h-32c-8.832 0-16 7.168-16 16v384c0 8.832 7.168 16 16 16h32c8.832 0 16-7.168 16-16v-384c0-8.832-7.168-16-16-16z"
          data-original="#000000"
          xmlns="http://www.w3.org/2000/svg"
        />
      </g>
    </svg>
  );
}

export default SvgBackSound;
