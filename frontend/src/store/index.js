import GeneralReducer, {
  GeneralReducerName,
} from '../redux/general/index';
import {applyMiddleware, createStore, combineReducers} from 'redux';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  [GeneralReducerName]: GeneralReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
