import {SET_BACKGROUND, SET_BING_RESULTS, SET_GOOGLE_RESULTS, SET_VIEW, SET_WORD_TO_SEARCH} from "./actionTypes";
import {VIEWS_CONSTANTS} from "../../containers/navigator/constants";

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const initialState = {
  view: VIEWS_CONSTANTS.searchPage,
  backgroundImage: getRandomInt(0, 6),
  googleResults: false,
  bingResults: false,
  searchWord: ''
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_VIEW:
      return {...state, view: action.payload};
    case SET_BACKGROUND:
      return {...state, backgroundImage: action.payload}
    case SET_GOOGLE_RESULTS:
      return {...state, googleResults: action.payload}
    case SET_BING_RESULTS:
      return {...state, bingResults: action.payload}
    case SET_WORD_TO_SEARCH:
      return {...state, searchWord: action.payload}
    default:
      return state;
  }
}
