import {SET_BACKGROUND, SET_BING_RESULTS, SET_GOOGLE_RESULTS, SET_VIEW, SET_WORD_TO_SEARCH} from './actionTypes';
import {GeneralReducerName} from "./index";
import {VIEWS_CONSTANTS} from "../../containers/navigator/constants";

export const NextBackgroundImage = () => async (dispatch, getState) => {
  let backgroundImg = getState()[GeneralReducerName].backgroundImage;
  if(backgroundImg !== 6){
    dispatch({type: SET_BACKGROUND, payload: backgroundImg + 1});
  } else {
    dispatch({type: SET_BACKGROUND, payload: 0});
  }
  return Promise.resolve();
};

export const PrevBackgroundImage = () => async (dispatch, getState) => {
  let backgroundImg = getState()[GeneralReducerName].backgroundImage;
  if(backgroundImg !== 0){
    dispatch({type: SET_BACKGROUND, payload: backgroundImg - 1});
  } else {
    dispatch({type: SET_BACKGROUND, payload: 6});
  }
  return Promise.resolve();
};

export const SetGoogleResults = (Results) => async (dispatch) => {
  dispatch({type: SET_GOOGLE_RESULTS, payload: Results});
  return Promise.resolve();
};
export const SetBingResults = (Results) => async (dispatch) => {
  dispatch({type: SET_BING_RESULTS, payload: Results});
  return Promise.resolve();
};
export const SetView = (view) => async (dispatch) => {
  dispatch({type: SET_VIEW, payload: view});
  return Promise.resolve();
};
export const SetSearchWord = (word) => async (dispatch) => {
  dispatch({type: SET_WORD_TO_SEARCH, payload: word});
  return Promise.resolve();
};
export const BackSearch = () => async (dispatch) => {
  dispatch({type: SET_GOOGLE_RESULTS, payload: false})
  dispatch({type: SET_BING_RESULTS, payload: false})
  dispatch({type: SET_WORD_TO_SEARCH, payload: ''})
  dispatch({type: SET_VIEW, payload: VIEWS_CONSTANTS.searchPage})
}
