import NAME from "./constants";

export const SET_VIEW = NAME + "/SET_VIEW";
export const SET_BACKGROUND = NAME + "/SET_BACKGROUND";
export const SET_GOOGLE_RESULTS = NAME + "/SET_GOOGLE_RESULTS";
export const SET_BING_RESULTS = NAME + "/SET_BING_RESULTS";
export const SET_WORD_TO_SEARCH = NAME + "/SET_WORD_TO_SEARCH";
