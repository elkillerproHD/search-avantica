## Thanks
to start the project

    yarn start
    yarn server // IN ANOTHER TAB

I want to thank you for waiting for the project. It took longer than expected because I wanted to deliver much more than expected.


**Challenge:**
- Create web application using react / redux which provides search aggregation functionality
- there should be a submit button, search textbox where user can enter anything, and a dropdown with options google bing, and both
- on submit, App should use existing google / bing search endpoint to fetch data and render
- when selected both, it should get data from both search engine and render.

In summary, the challenge was to create a search bar in which when pressing search, a query is made with the words entered in the search bar, to google, bing or both (according to what the user chooses) and the results on screen.

## What features did I add?


![enter image description here](https://cdn.zeplin.io/5f68f1051a5239171f37c6d6/screens/b4b68bd4-27da-48bf-9903-4ff81f06db17.png)

As you can see from this little desing I created, I wanted to add a news feed, the ability to choose the background image, and a music player to which I added a spectrum analyzer.

**Final result:**
![enter image description here](https://firebasestorage.googleapis.com/v0/b/avantica-search.appspot.com/o/Captura%20de%20pantalla%20de%202020-10-27%2021-14-04.png?alt=media&token=ed9fe3bc-3d2d-4df9-a523-b02bdecc481b)

## Docs

 - [Problems](#problems)
 - Components
 - Containers
 - Redux

### Problems
This challenge at first glance seems simple and it would be if it were not because there is no api to use google search and to use bing search you must be able to handle a minimum with Microsoft Azure.

I want to make it quite clear that there is no google api for the google search engine, for example https://developers.google.com/custom-search is not an api to use the original search engine, which is a custom search bar for each user and that You have to place the results you want to use from web pages that you already know.

Making that clear, the problem that is generated with this is that the only alternative that remains is to read the html that google returns, analyze it, modify it and return it in the form of an api through an intermediary backend since Google does not allow queries in its search engine from the browser by Cors policies

That is why I was forced to use express and fetch, returning the parsed html code and finishing modifying it in the frontend since the browser facilitates playing with html much easier than in nodejs.

### Components

This projects has 4 main components.
1- Audio Player
2- Background Image Picker
3- The News Feed
4- Search Bar

PD: The audio player has a real spectrum analyzer
PD2: I chose that instead of a dropdown, it is a multiple choice button to choose between google, bing or both

### Containers

This projects has 5 Smart Components.
1- The bing results
2- The google results
3- The google and bing results
4- The search bar page
5- The navigator

PD: You can navigate between the screen really easy with a redux state

### Redux
I did not know what redux configuration to use so I used the one that was closest to a personal repository.

In a folder with the name of the section to use (in this case the general section) the following files are created:

 - action.js // Here you create the action with redux thunk
 - actionTypes.js // Here you create the name of the action that will be sent to the reducer
 - constants.js // The name of the section
 - index.js // Here you import and export the important things of the folder
 - reducer.js // Here you create the reducer


## Ending
I would have loved to upload the web to firebase so they don't have to download such a heavy repository but I didn't have more time this week.

Thank for reading. If you have more questions please, contact me. See you!
