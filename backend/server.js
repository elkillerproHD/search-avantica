
const express = require('express')
const app = express()
const port = 3001

const fetch = require('node-fetch');
const cors = require('cors')({origin: true});
const https = require('https')
const CognitiveServicesCredentials = require('@azure/ms-rest-azure-js').CognitiveServicesCredentials;
const {WebSearchAPIClient} = require('@azure/cognitiveservices-websearch');

app.get('/google', (request, response) => {
  const query = request.query["q"];

  response.set('Access-Control-Allow-Origin', '*');
  fetch("https://google.com/search?q="+query, {
    mode: 'no-cors'
  }).then(function (response) {
    // The API call was successful!
    return response.text();
  }).then(function (html) {
    const text = html.toString();
    const startCut = text.indexOf('<div id="main">')
    const finalCut = text.indexOf('<footer>')
    let mainContent = text.substring(startCut, finalCut)
    response.json({
      content: mainContent
    });
  }).catch(function (err) {
    // There was an error
    console.warn('Something went wrong.', err);
  });
})
app.get('/bing', (request, response) => {
  const query = request.query["q"];

  response.set('Access-Control-Allow-Origin', '*');
  https.get({
    hostname: 'api.cognitive.microsoft.com',
    path:     '/bing/v7.0/search?q='+query,
    headers:  { 'Ocp-Apim-Subscription-Key': 'd1c931f9ed55476a8331a0a596af8a28' },
  }, res => {
    let body = ''
    res.on('data', part => body += part)
    res.on('end', () => {
      for (var header in res.headers) {
        if (header.startsWith("bingapis-") || header.startsWith("x-msedge-")) {
          console.log(header + ": " + res.headers[header])
        }
      }
      response.json({
        content: JSON.parse(body)
      });
    })
    res.on('error', e => {
      console.warn('Something went wrong.', e.message);

    })
  })
})
app.get('/bing_google', (request, response) => {
  const query = request.query["q"];

  response.set('Access-Control-Allow-Origin', '*');
  fetch("https://google.com/search?q="+query, {
    mode: 'no-cors'
  }).then(function (response) {
    // The API call was successful!
    return response.text();
  }).then(function (html) {
    const text = html.toString();
    const startCut = text.indexOf('<div id="main">')
    const finalCut = text.indexOf('<footer>')
    let google = text.substring(startCut, finalCut)
    https.get({
      hostname: 'api.cognitive.microsoft.com',
      path:     '/bing/v7.0/search?q='+query,
      headers:  { 'Ocp-Apim-Subscription-Key': 'd1c931f9ed55476a8331a0a596af8a28' },
    }, res => {
      let body = ''
      res.on('data', part => body += part)
      res.on('end', () => {
        for (var header in res.headers) {
          if (header.startsWith("bingapis-") || header.startsWith("x-msedge-")) {
            console.log(header + ": " + res.headers[header])
          }
        }
        response.json({
          google: google,
          bing: JSON.parse(body)
        });
      })
      res.on('error', e => {
        console.warn('Something went wrong.', e.message);

      })
    })
  }).catch(function (err) {
    // There was an error
    console.warn('Something went wrong.', err);
  });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
